import angular from 'angular';

class ObjectsStorage {
    constructor($http, $q) {
        this.listeners = {};

        this.$load = function (type, url) {
            $http.get(url).then(response => {
                this[type] = response.data.constructor === Array ? response.data : [];
                this.getListeners(type).forEach(handler => handler.call(this, this[type]));
            })
        };

        this.$get = function(type) {
            let deferred = $q.defer();

            if (this.hasOwnProperty(type)) {
                deferred.resolve(this[type]);
                return deferred.promise;
            }

            let self = this;
            let handler = function() {
                self.removeOnLoadListener(type, handler);
                deferred.resolve(this[type]);
            };

            this.addOnLoadListener(type, handler);
            return deferred.promise;
        }
    }

    addOnLoadListener(type, handler) {
        if (handler.constructor !== Function) return;

        this.getListeners(type).push(handler);
    }

    removeOnLoadListener(type, handler) {
        let listeners = this.getListeners(type);
        this.listeners[type] = listeners.filter(listener => listener !== handler);
    }

    getListeners(type) {
        if (!this.listeners.hasOwnProperty(type)) this.listeners[type] = [];
        return this.listeners[type];
    }
}

ObjectsStorage.$inject = ['$http', '$q'];

export default angular.module('app.services.objects-storage', [])
    .service('objectsStorage', ObjectsStorage)
    .name;