/**
 * @readonly
 * @enum {int}
 */
var VehicleType = {
    BUS: 1,
    TRUCK: 2
};

export default VehicleType;
