/**
 * @readonly
 * @enum {int}
 */
var PurchaseOptions = {
    ONE_TIME_PAYMENT: 1,
    LEASING: 2
};

export default PurchaseOptions;
