/**
 * @readonly
 * @enum {int}
 */
var RechargingAbility = {
    NO: 1,
    NIGHT_ONLY: 2,
    INTRADAY: 3
};

export default RechargingAbility;
