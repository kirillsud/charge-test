import angular from 'angular';

import config from './app.config';

// features
import calculator from './features/calculator';

angular.module('app', [calculator])
    .config(config);